'use strict'

var chai = require('chai');
var expect = chai.expect;

// var tweets = require('../app/controllers/v1/tweets');

function getTweet(i) {
    if (!i) {
        return 0;
    }
    return i;
}
describe('getTweet', function () {
    it('getTweet() should return 0 if no items are passed in', function () {
        expect(getTweet()).to.equal(0)
    })
});
