import express from 'express'
import fs from 'fs'
const env = process.env.NODE_ENV || 'development';
import mongoose from 'mongoose';

mongoose.connect(process.env.MONGO_DB_HOST);

const models_path = __dirname + '/app/models/';

fs.readdirSync(models_path).forEach(file => {
  require(models_path + '/' + file);
});

const app = express();
require('./app/middlewares')(app)
require('./app/routes/')(app);
app.use('*', function (req, res, next){
  res.status(404).send();
})

const port = process.env.PORT || 3000;
app.listen(port);
console.log('Express app started on port '+port);
