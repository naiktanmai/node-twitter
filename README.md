### Overview
We include scripts in this repo to get this API running on a virtual machine using [VitrualBox](https://www.virtualbox.org/) and [Vagrant](https://www.vagrantup.com/).

The VM is automatically provisioned using [Ansible](https://www.ansible.com/) and should require very few manual steps.

If these ports are not available on your local machine (e.g. if another VM is forwarding to them), or if you just want to change them, you can change them in your local Vagrantfile


### Developer Environment Setup

##### Preliminaries:
  - Install VirtualBox: https://www.virtualbox.org
  - Install Vagrant: https://www.vagrantup.com
  - Install Ansible: http://www.ansible.com

##### VM Creation

  - `git clone {url}`
  - `cd {folder}`
  - `vagrant up` This should take a few mins

If there are any errors regarding ports, follow the instructions above about port forwarding. Otherwise, you will need to debug or get into contact with us.

Now, the VM should be running, and the applications should be accessible at the following addresses:

**API Endpoint**: http://192.168.33.44:8080

**However, whenever you reboot your local machine, you will need to restart the VM and some services inside of it:**

  - Start your VM: `vagrant up`
  - SSH into the VM: `vagrant ssh`
  - Password: `vagrant`

##### VM Admin and Troubleshooting

  - SSH into your VM: `vagrant ssh`
