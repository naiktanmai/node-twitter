import express from 'express'
import bodyParser from 'body-parser'
import morgan from 'morgan'

module.exports = (app) => {
  app.set('showStackError', true);

  app.set('superSecret', process.env.SECRET)

  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  app.use(morgan('dev'));

};
