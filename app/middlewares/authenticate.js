const jwt = require('jsonwebtoken')
import _ from 'underscore'

export const authenticate = (req, res, next) => {
        const headers = _.extend({}, req.headers)
        const token = req.headers['x-auth-token']
        if (!token) {
            return res.status(401).send()
        }
        jwt.verify(token, process.env.SECRET, (err, decoded) => {
          if (err) {
            return res.status(401).send()
          }
          req.headers['x-user-id'] = decoded['_doc']['_id']
          return next()
        }
      )
}
