const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Tweet Schema
const TweetSchema = new Schema({
    body: {type: String, default: '', trim: true},
    user: {type: Schema.ObjectId, ref: 'User'},
    version: { type: Number, default: 0 },
    history: [{createdAt: { type: Date, default: Date.now }, body: {type: String, default: '', trim: true}}],
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});

mongoose.model('Tweet', TweetSchema);
