const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const crypto = require('crypto');

// ## Define UserSchema
const UserSchema = new Schema({
    email: String,
    username: String,
    password: String
});

mongoose.model('User', UserSchema);
