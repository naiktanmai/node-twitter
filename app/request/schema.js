const Joi = require('joi')

export const CreateUserSchema = Joi.object().keys({
        username: Joi.string().required(),
        email: Joi.string().email().required(),
        password: Joi.string().required()
    });

export const AuthenticateUserSchema = Joi.object().keys({
        id: Joi.string().required(),
        password: Joi.string().required()
    });

export const CreateTweetSchema = Joi.object().keys({
        body: Joi.string().required()
    });

export const UpdateTweetSchema = Joi.object().keys({
        body: Joi.string().required()
    });
