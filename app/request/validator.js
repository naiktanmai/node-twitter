import Joi from 'joi'
import util from 'util'
import _ from 'lodash'

let BadRequestError = (errors) => {
    Error.captureStackTrace(this, this.constructor)
    this.name = 'BadRequestError'
    this.message = 'Bad Request Error'
    this.errors = errors
}

util.inherits(BadRequestError, Error)

export const validate = (schema) => (
    (req, res, next) => {
        let body = _.extend({}, req.body)

        Joi.validate(body, schema, {abortEarly: false}, (err, schemaResult) => {
            if (err) {
                let details = []
                err.details.forEach( d => {
                    details.push({message: d.message, path: d.path})
                })

                return res.status(500).json(details)
            }

            req.schema = schemaResult
            return next()
        })
    }
)

export { BadRequestError }
