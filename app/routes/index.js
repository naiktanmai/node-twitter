const users = require('../controllers/v1/users')
const tweets = require('../controllers/v1/tweets')

import {validate} from '../request/validator'
import {CreateUserSchema, AuthenticateUserSchema, CreateTweetSchema, UpdateTweetSchema} from '../request/schema'
import {authenticate} from '../middlewares/authenticate'

module.exports = (app,auth) => {
  app.get('/', function(req, res) {
    res.status(200).send();
  });

  app.post('/user', validate(CreateUserSchema), users.createUser)
  app.get('/user/:id', authenticate, users.getUserById)
  app.get('/user', authenticate, users.getUsers)
  app.post('/user/auth', validate(AuthenticateUserSchema), users.authenticateUser)

  app.post('/tweet',authenticate, validate(CreateTweetSchema), tweets.createTweet)
  app.get('/tweet/:id',authenticate, tweets.getTweet)
  app.patch('/tweet/:id',authenticate, validate(UpdateTweetSchema), tweets.updateTweet)
};
