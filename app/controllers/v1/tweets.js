import mongoose from 'mongoose'
import bluebird from 'bluebird'

const User = mongoose.model('User')
const Tweet = mongoose.model('Tweet')

bluebird.promisifyAll(mongoose)

exports.createTweet = (req, res) => {
  const user = req.headers['x-user-id']
  const tweetBody = req.body.body
  let tweet = new Tweet({
    body:tweetBody,
    user
  })

  tweet.saveAsync()
  .then(data => res.json(data))
  .catch(error => res.status(500).json(error))
}

exports.getTweet = (req, res) => {
  const _id = req.params.id
  Tweet.findOneAsync({
    _id
  })
  .then(data => res.json(data))
  .catch(error => res.status(404).json(error))
}

exports.updateTweet = (req, res) => {
  const _id = req.params.id
  const tweetBody = req.body.body
  Tweet.findOneAsync({
    _id
  })
  .then( data => {
    if (data.user != req.headers['x-user-id']) {
      return res.send(403)
    }

    const originalBody = data.body

    if (originalBody == tweetBody) {
        return res.status(409).send()
    }

    Tweet.update({
      _id
    }, {
      '$addToSet': {
          'history': {
              'body': originalBody
          }
        },
      '$set': {
        'body': tweetBody
      }
    },
    { upsert: false })
    .exec()
    .then(data => res.status(204).send())
    .catch( error => res.status(404).send(error))
  })
  .catch( error => res.status(404).send(error))
}
