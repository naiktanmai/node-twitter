import mongoose from 'mongoose'
import bluebird from 'bluebird'
const jwt = require('jsonwebtoken')
const User = mongoose.model('User');

bluebird.promisifyAll(mongoose)

exports.createUser = (req, res) => {
    const body = req.body
    const user = new User(body)
    user.saveAsync()
    .then( data => res.status(200).send(data))
    .catch(error => res.status(500).send(error))
}

exports.getUserById = (req, res) => {
    const _id = req.params.id
    User.findOneAsync({
      _id
    })
    .then( data => res.json(data))
    .catch( data => res.status(404).send())
}

exports.getUsers = (req, res) => {
    User.findAsync()
    .then( data => res.json(data))
    .catch( data => res.status(500).send())
}

exports.authenticateUser = (req, res) => {
    const body = req.body
    const _id = body.id
    User.findOneAsync({
        _id
    })
    .then( (data) => {
        if (body.password != data.password) {
            res.status(400).json({ message: 'Authentication failed. Wrong password.' })
            return
        }
        const token = jwt.sign(data, process.env.SECRET, {
          expiresIn: '24h' // expires in 10 hours
        });

        res.json({
          expiresIn: 86400,
          token
        });

    })
    .catch(error => res.status(404).send(error))
}
